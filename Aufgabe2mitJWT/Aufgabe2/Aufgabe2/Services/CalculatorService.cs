﻿using Grpc.Core;
using Aufgabe2.Protos;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace Aufgabe2.Services
{
    public class CalculatorService: Calculator.CalculatorBase
    {
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public override Task<Result> Div(Arguments request, ServerCallContext context)
        {
            var principal = context.GetHttpContext().User;
            Console.WriteLine($"Callee:{principal.Identity.Name}");

            if (request.Arg2 != 0)
            {
                return Task.FromResult(
                    new Result()
                    {
                        Value = request.Arg1/request.Arg2
                    });
                
            }

            return Task.FromResult(
                new Result()
                {
                    Value = 0
                });
        }
    }
}
