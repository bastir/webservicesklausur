using Aufgabe2.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

var builder = WebApplication.CreateBuilder(args);

// Additional configuration is required to successfully run gRPC on macOS.
// For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682

// Add services to the container.
builder.Services.AddGrpc();
builder.Services.AddAuthorization();

const String JwtSigninKey = "075af198-356e-4ef8-bb72-35c5521b64db";
var securitykey = new SymmetricSecurityKey(Guid.Parse(JwtSigninKey).ToByteArray());

builder.Services.AddAuthentication(options => { options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme; })

    .AddJwtBearer(options =>

    {

        options.TokenValidationParameters =

            new TokenValidationParameters

            {

                ValidateAudience = false,

                ValidateIssuer = false,

                ValidateActor = false,

                ValidateLifetime = true,

                IssuerSigningKey = securitykey

            };

    });

var app = builder.Build();
app.UseAuthorization();
app.UseAuthentication();
// Configure the HTTP request pipeline.
app.MapGrpcService<CalculatorService>();
app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

app.Run();
