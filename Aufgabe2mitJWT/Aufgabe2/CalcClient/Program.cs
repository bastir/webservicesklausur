﻿using Grpc.Net.Client;
using CalcClient.Protos;
using Grpc.Core;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;



var token = GenerateToken(Environment.UserName);



var credentials = CallCredentials.FromInterceptor((context, metadata) =>

{

    if (!string.IsNullOrEmpty(token))

    {

        metadata.Add("Authorization", $"Bearer {token}");

    }

    return Task.CompletedTask;

});

double arg1 = Convert.ToDouble(Console.ReadLine());
double arg2 = Convert.ToDouble(Console.ReadLine());
using var channel = GrpcChannel.ForAddress("https://localhost:7024", new GrpcChannelOptions
{
    Credentials = ChannelCredentials.Create(new SslCredentials(), credentials)
});

var client = new Calculator.CalculatorClient(channel);

var result = client.Div( new Arguments()
{
    Arg1 = arg1,
    Arg2 = arg2,
});
Console.WriteLine(result.Value);
Console.ReadLine();

string GenerateToken(string name)
{
    const String JwtSigninKey = "075af198-356e-4ef8-bb72-35c5521b64db";
    var securitykey = new SymmetricSecurityKey(Guid.Parse(JwtSigninKey).ToByteArray());
    JwtSecurityTokenHandler jwtTokenHandler = new JwtSecurityTokenHandler();

    var claims = new[] { new Claim(ClaimTypes.Name, name) };

    var credentials = new SigningCredentials(securitykey, SecurityAlgorithms.HmacSha256);

    var token = new JwtSecurityToken(
        "ExampleServer",
        "ExampleClients",
        claims,
        expires: DateTime.Now.AddSeconds(60),
        signingCredentials: credentials
    );
    return jwtTokenHandler.WriteToken(token);
}

Console.ReadLine();




