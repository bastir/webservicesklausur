﻿using Grpc.Core;
using Aufgabe2.Protos;

namespace Aufgabe2.Services
{
    public class CalculatorService: Calculator.CalculatorBase
    {
        public override Task<Result> Div(Arguments request, ServerCallContext context)
        {
            if (request.Arg2 != 0)
            {
                return Task.FromResult(
                    new Result()
                    {
                        Value = request.Arg1/request.Arg2
                    });
                
            }

            return Task.FromResult(
                new Result()
                {
                    Value = 0
                });
        }
    }
}
