﻿using Grpc.Net.Client;
using CalcClient.Protos;
using Grpc.Core;



double arg1 = Convert.ToDouble(Console.ReadLine());
double arg2 = Convert.ToDouble(Console.ReadLine());
using var channel = GrpcChannel.ForAddress("http://localhost:5024", new GrpcChannelOptions
{
    Credentials = ChannelCredentials.Insecure
});

var client = new Calculator.CalculatorClient(channel);

var result = client.Div( new Arguments()
{
    Arg1 = arg1,
    Arg2 = arg2,
});
Console.WriteLine(result.Value);
Console.ReadLine();




