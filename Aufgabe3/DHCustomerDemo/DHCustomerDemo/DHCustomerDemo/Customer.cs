namespace CustomerDemo;

public class Customer
{
    public string Id { get; }
    public string Name { get; }
    public string Street { get; set; }
    public string ZipCode { get; set; }
    public string City { get; set; }
    public string Country { get; set; }
    public string PictureUrl { get; }
    public bool IsFavorite { get;  set; }

    public Customer(string id, string name, string street, string zipCode, string city, string country, string pictureUrl)
    {
        Id = id;
        Name = name;
        Street = street;
        ZipCode = zipCode;
        City = city;
        Country = country;
        PictureUrl = pictureUrl;
        IsFavorite = false;
    }
}