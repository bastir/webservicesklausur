﻿using CustomerDemo.Hypermedia;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Action = CustomerDemo.Hypermedia.Action;

namespace CustomerDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {

        private VolatileCustomerRepository m_Repository;

        public CustomersController(VolatileCustomerRepository repository)
        {
            m_Repository = repository;
        }

        [HttpGet(Name = "GetAllCustomers")]
        public async Task<IActionResult> GetAll()
        {
            var siren = new Siren();
            var allCustomers = await m_Repository.GetAll();

            siren.Class.Add("customers");
            foreach (var customer in allCustomers)
            {
                var entity = new EmbeddedRepresentation();
                entity.Class.Add("customerOverview");
                entity.Properties.Add(new Property() { Name = "Name", Value = customer.Name });
                entity.Properties.Add(new Property() { Name = "City", Value = customer.City });

                entity.Links.Add(new Link()
                {
                    Relation = new List<string>() { "self" },
                    Href = Url.Link("GetCustomerById", new { id = customer.Id })!

                });


                siren.Entities.Add(entity);
            }
            siren.Links.Add(new Link()
            {
                Relation = new List<string>() { "self" },
                Href = Url.Link("GetAllCustomers", null)!
            });

            return Ok(siren);
        }

        [HttpGet("{id}", Name = "GetCustomerById")]
        public async Task<IActionResult> GetById(string id)
        {
            try
            {
                var customer = await m_Repository.GetById(id);
                var siren = new Siren();
                siren.Class.Add("Customer");
                siren.Properties.Add(new Property { Name = "Name", Value = customer.Name });
                siren.Properties.Add(new Property { Name = "Street", Value = customer.Street });
                siren.Properties.Add(new Property { Name = "ZipCode", Value = customer.ZipCode });
                siren.Properties.Add(new Property { Name = "City", Value = customer.City });
                siren.Properties.Add(new Property { Name = "Country", Value = customer.Country });
                siren.Properties.Add(new Property { Name = "IsFavorite", Value = customer.IsFavorite });

                if (!customer.IsFavorite)
                {
                    siren.Actions.Add(new Action()
                    {
                        Name = "MarkAsFavorite",
                        Method = "POST",
                        Fields = new List<object>(){ "CustomerReference" },
                        Href = Url.Link("MarkAsFavorite", null)!
                    });
                }
                else
                {
                    siren.Actions.Add(new Action()
                    {
                        Name = "DeleteFromFavorites",
                        Method = "DELETE",
                        Fields = new List<object>() { "CustomerReference" },
                        Href = Url.Link("DeleteFromFavorites", null)!
                    });

                }

                string link = Url.Link("AddMoves", null);
                siren.Actions.Add(new Hypermedia.Action()
                {
                    Name = "Move",
                    Method = "POST",
                    Fields = new List<object>() {"MoveReference"},
                    Href = Url.Link("AddMoves",null)!
                });
               


                siren.Links.Add(new Link()
                {
                    Relation = new List<string>() { "self" },
                    Href = Url.Link("GetCustomerById", new { id = customer.Id })!
                });
                return Ok(siren);
            }
            catch (IndexOutOfRangeException)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] NewCustomerData c)
        {
            var customer = new Customer(
                Guid.NewGuid().ToString(),
                c.Name,
                c.Street,
                c.ZipCode,
                c.City, c.Country,
                String.Empty);
            await m_Repository.Update(customer);

            var newCustomerUrl = Url.Link("GetCustomerById", new { id = customer.Id });

            return Created(newCustomerUrl!, null);
        }
    }
}
