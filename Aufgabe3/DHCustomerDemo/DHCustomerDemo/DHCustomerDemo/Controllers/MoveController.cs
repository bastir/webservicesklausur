﻿using Microsoft.AspNetCore.Mvc;

namespace CustomerDemo.Controllers
{
    [Route("api/customers/{customerId:guid}/[controller]")]
    [ApiController]
    public class MoveController : ControllerBase
    {
        [HttpGet (Name = "GetMoves")]
        public IEnumerable<Move> Get([FromRoute] string customerId)
        {
            return VolatileCustomerRepository.GetAllMoves(customerId);
        }
        [HttpPost (Name = "AddMoves")]
        public IActionResult Post([FromRoute] string customerId, [FromBody] Move move)
        {
            VolatileCustomerRepository.AddMove(customerId, move);
            return new CreatedAtRouteResult(Routes.GetCustomer, new { id = customerId }, null);
        }
    }

    public static class Routes
    {
        public const string GetCustomer = "GetCustomerById";
    }

}

