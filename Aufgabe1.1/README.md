﻿# Web Services Client

## Aufgabenstellung
In der Cloud ist ein Echo Webservice vorhanden. D.h. Sie geben eine Zeichenfolge ein und bekommen dieselbe als Ergebnis wieder zurück. 

## Vorgehen
1. Neue Console App .Net Framework erstellen
   - Project Name: `EchoClient`
   - Solution: `Echo`
1. Neue Service Reference hinzufügen
   - Rechtsklick `Reference` -> `Add Service Reference` -> WSDL Url eingeben
   - Name: `EchoServiceReference`
2. `Program.cs` bearbeiten
   - Über die Service Reference einen Client erstellen `var client = new ...;`
   - Aufgabe ausführen (hier Addition)
   - Ergebnis Ausgeben
   - Klient schließen
