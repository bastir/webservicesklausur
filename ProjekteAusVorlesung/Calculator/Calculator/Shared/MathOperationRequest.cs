﻿namespace Shared;

public record MathOperationRequest(MathOperation Operation, double[] Operands);
