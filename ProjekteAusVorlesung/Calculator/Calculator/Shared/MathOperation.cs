﻿namespace Shared;

public enum MathOperation {
	Add, Subtract, Multiply, Divide
}
