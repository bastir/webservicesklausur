﻿using Shared;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapPost("", (MathOperationRequest Request) => Request.Operation switch {
    MathOperation.Add       => Results.Ok(Request.Operands?.Aggregate((accumulator, next) => accumulator + next)),
    MathOperation.Subtract  => Results.Ok(Request.Operands?.Aggregate((accumulator, next) => accumulator - next)),
    MathOperation.Multiply  => Results.Ok(Request.Operands?.Aggregate((accumulator, next) => accumulator * next)),
    MathOperation.Divide    => Results.Ok(Request.Operands?.Aggregate((accumulator, next) => accumulator / ((next == 0) ? 1 : next))),
    _                       => Results.BadRequest("invalid operator index")
})
.Produces<double>(StatusCodes.Status200OK)
.Produces(StatusCodes.Status400BadRequest);

app.Run();
