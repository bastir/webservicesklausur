﻿using Microsoft.AspNetCore.Mvc;

namespace CalculatorController.Controllers;

//public record Request(double[] Operands);

[ApiController]
public class CalculatorController: ControllerBase {

    [HttpPost("add")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(double))]
    public ActionResult<double> Add(double[] Operands) {
        return Ok(Operands.Aggregate((accumulator, next) => accumulator + next));
    }

    [HttpPost("subtract")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(double))]
    public ActionResult<double> Subtract(double[] Operands) {
        return Ok(Operands.Aggregate((accumulator, next) => accumulator - next));
    }

    [HttpPost("multiply")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(double))]
    public ActionResult<double> Multiply(double[] Operands) {
        return Ok(Operands.Aggregate((accumulator, next) => accumulator * next));
    }

    [HttpPost("divide")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(double))]
    public ActionResult<double> Divide(double[] Operands) {
        return Ok(Operands.Aggregate((accumulator, next) => accumulator / ((next == 0) ? 1 : next)));
    }

}
