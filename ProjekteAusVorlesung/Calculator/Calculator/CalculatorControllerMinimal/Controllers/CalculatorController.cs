﻿using Shared;
using Microsoft.AspNetCore.Mvc;

namespace CalculatorController.Controllers;

[ApiController]
public class CalculatorController: ControllerBase {

    [HttpPost("")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(double))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public ActionResult<double> Calculate(MathOperationRequest Request) {
        return Request.Operation switch {
            MathOperation.Add       => Ok(Request.Operands?.Aggregate((accumulator, next) => accumulator + next)),
            MathOperation.Subtract  => Ok(Request.Operands?.Aggregate((accumulator, next) => accumulator - next)),
            MathOperation.Multiply  => Ok(Request.Operands?.Aggregate((accumulator, next) => accumulator * next)),
            MathOperation.Divide    => Ok(Request.Operands?.Aggregate((accumulator, next) => accumulator / ((next == 0) ? 1 : next))),
            _                       => BadRequest("invalid operator index")
        };
    }

}

