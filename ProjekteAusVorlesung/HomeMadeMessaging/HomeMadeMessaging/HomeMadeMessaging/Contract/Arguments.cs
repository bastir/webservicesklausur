﻿namespace Contract
{
    public class Arguments
    {
        public double Arg1 { get; set; }
        public double Arg2 { get; set; }
    }

    //public record Arguments(double Arg1, double Arg2);
}