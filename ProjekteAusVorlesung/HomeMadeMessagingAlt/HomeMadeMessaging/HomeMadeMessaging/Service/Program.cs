﻿using Contract;

using Azure.Messaging.ServiceBus;
using Microsoft.Azure.Cosmos.Table;

var serviceBusConnectionString = "Endpoint=sb://dhmessagingam.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=htnmImUsazEJwWnyOoFujRIjsOxxZ57YiLe0zTepeNo="; //Don't do this at home
var storageAccountConnectionString = "DefaultEndpointsProtocol=https;AccountName=calculationresultsam;AccountKey=e8bL0wlhNnQZm2NixjsXkIiIBYDLY3SJl7kGOVODFgt9b81Xs2npdtdcGW2Nssr2cdfz33xOcnmdSpEsmRUG0g==;EndpointSuffix=core.windows.net";
var queueName = "add";

await using var serviceBusClient = new ServiceBusClient(serviceBusConnectionString);
var options = new ServiceBusReceiverOptions();
await using var receiver = serviceBusClient.CreateReceiver(queueName, options);

while (true)
{
    var message = await receiver.ReceiveMessageAsync();

    if (message == null)
    {
        await Task.Delay(100);
        continue;
    }

    var correlation = message.CorrelationId;
    var json = System.Text.Encoding.UTF8.GetString(message.Body);
    var arguments = Newtonsoft.Json.JsonConvert.DeserializeObject<Arguments>(json);
    var result = new Result() { Value = arguments.Arg1 + arguments.Arg2 };

    var storageAccount = CloudStorageAccount.Parse(storageAccountConnectionString);
    var tableClient = storageAccount.CreateCloudTableClient();
    var table = tableClient.GetTableReference("result");


    Console.WriteLine($"Message {correlation}: {arguments.Arg1} + {arguments.Arg2}");
    await receiver.CompleteMessageAsync(message);
}









Console.ReadLine();
