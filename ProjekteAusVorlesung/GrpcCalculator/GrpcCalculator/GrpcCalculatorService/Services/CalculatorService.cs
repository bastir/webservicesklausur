using Grpc.Core;
using GrpcCalculatorService.Proto;

namespace GrpcCalculatorService.Services;

public class CalculatorService : Calculator.CalculatorBase
{
    public override Task<Result> Add(Arguments request, ServerCallContext context)
    {
        //var r = new Result();
        //r.Value = request.Arg1 + request.Arg2;
        //return Task.FromResult(r);

        return Task.FromResult(
            new Result()
            {
                Value = request.Arg1 + request.Arg2
            }
        );
    }
}