﻿// See https://aka.ms/new-console-template for more information

using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Grpc.Net.Client;
using GrpcCalculatorClient.Proto;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualBasic.FileIO;


Console.WriteLine("Press enter to start");
Console.ReadLine();

var token = GenerateToken(Environment.UserName);


using var channel = GrpcChannel.ForAddress("https://localhost:7076");

var client = new Calculator.CalculatorClient(channel);

var result = client.Add(new Arguments()
{
    Arg1 = 1,
    Arg2 = 2
});


string GenerateToken(string name)
{
    const string jwtSigningKey = "f0e7846c-a40c-40b3-93a2-8c6c55c8727c";
    var securityKey = new SymmetricSecurityKey(Guid.Parse(jwtSigningKey).ToByteArray());
    var jwtTokenHandler = new JwtSecurityTokenHandler();


    var claims = new[] { new Claim(ClaimTypes.Name, name) };
    var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
    var token = new JwtSecurityToken(
        "ExampleServer", 
        "ExampleClients", 
        claims, 
        expires: DateTime.Now.AddSeconds(60), 
        signingCredentials: credentials);

    return jwtTokenHandler.WriteToken(token);
}





Console.WriteLine(result.Value);








