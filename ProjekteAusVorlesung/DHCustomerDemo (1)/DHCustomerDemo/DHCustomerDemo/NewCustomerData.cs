namespace CustomerDemo;

public record NewCustomerData(string Name, string Street, string ZipCode, string City, string Country);