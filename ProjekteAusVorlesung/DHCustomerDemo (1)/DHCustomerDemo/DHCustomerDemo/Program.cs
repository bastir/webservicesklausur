using CustomerDemo;
using CustomerDemo.Hypermedia;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddSingleton<VolatileCustomerRepository>();

builder.Services.AddMvc(
    options =>
    {
        //options.OutputFormatters.Clear();
        options.OutputFormatters.Add(new CustomerImageFormatter());
        options.OutputFormatters.Add(new SirenFormatter());
    });

var app = builder.Build();

// Configure the HTTP request pipeline.

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
