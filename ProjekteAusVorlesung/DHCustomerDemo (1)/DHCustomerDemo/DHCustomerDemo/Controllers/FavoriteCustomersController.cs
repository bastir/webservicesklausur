﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CustomerDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FavoriteCustomersController : ControllerBase
    {
        private readonly VolatileCustomerRepository m_Repository;

        public FavoriteCustomersController(VolatileCustomerRepository repository)
        {
            m_Repository = repository;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var favorites = await m_Repository.GetAllFavorites();
            return Ok(new List<Customer>(favorites)); //Kostet Speicher, verhindert Enumerations Exception
        }

        [HttpPost(Name= "MarkAsFavorite")]
        public async Task<IActionResult> Post([FromBody] CustomerReference customerReference)
        {
            try
            {
                var id = customerReference.Id.Segments.Last(); //Nicht in der Praxis. Framework fragen
                var customer = await m_Repository.GetById(id);
                customer.IsFavorite = true; //Nicht Herren Lindner zeigen
                await m_Repository.Update(customer);
                var newCustomerUrl = Url.Link("GetCustomerById", new { id = customer.Id });
                return Created(newCustomerUrl!, null);
            }
            catch
            {
                return NotFound();
            }
        }
    }

    public record CustomerReference(Uri Id);

}
